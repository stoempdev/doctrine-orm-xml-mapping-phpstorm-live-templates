# doctrine-orm-xml-mapping-PhpStorm-live-templates

Very basic Doctrine ORM XML Mapping Live Templates to be used in PHPStorm...

![screenshot](https://gitlab.com/stoempdev/doctrine-orm-xml-mapping-phpstorm-live-templates/-/raw/master/2020-11-18_23-21.png?inline=false)


List of templates:

- dom.root
- dom.entity
- dom.embedded
- dom.embeddable
- dom.indexes
- dom.index
- dom.id
- dom.field
- dom.one-to-one
- dom.one-to-many
- dom.many-to-many
- dom.unique-constraints
- dom.unique
- dom.join-column
- dom.mapped-superclass
- dom.discriminator-column
- dom.discriminator-map
- dom.discriminator-mapping
- dom.lifecycle-callbacks
- dom.lifecycle-callback
- dom.many-to-one

## usage

Simply download `doctrine_orm_mapping.xml` and move it into your PHPStorm config.  

On Linux, this is located at:  
`~/.config/JetBrains/PhpStorm%VERSION%` (replace version with your version!)

The subfolder depends on your sync settings. 

- If you are syncing your settings through a repo, you should go here: `settingsRepository/repository/templates` 
- otherwise, here: `options/templates`

Restart PHPStorm and you should be all set.

## feel free to use

Pull Requests are more than welcome for improvements